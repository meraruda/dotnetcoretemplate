# .Net Core SPA Start Kit
This project is build with .NET Core and React, default frontend project is [mr0a30/react_startkit](https://gitlab.devpack.cc/mr0a30/react_startkit), developer can replace it with another React base project by their self.

### Environment
- .NET SDK 5.0.100+
- Node JS 8.11.1+
- npm 6.4.1+

### Develop
- Setting system enviroment parameter
	```
	set ASPNETCORE_ENVIRONMENT=Development	
	```
- Start your frontend project
	```
	cd DotNetCoreWebApplicationStartKit/frontend
	npm run start
	```
- Start .NET Core project
	```
	cd DotNetCoreWebApplicationStartKit
	dotnet watch run
	```
- Swagger document will be running at http://localhost:5000/swagger
- Your SPA web application can be access via both http://localhost:5000 and localhost:{ClientDevServerPort}
- Application status page: http://localhost:5000/hc-ui, (setting document reference: https://github.com/xabaril/AspNetCore.Diagnostics.HealthChecks)
- Background job monitor page: http://localhost:5000/quartz

### Test
- Frontend
	```
	cd DotNetCoreWebApplicationStartKit/frontend
	npm run test
	```
- Backend
	```
	cd UnitTestProject
	dotnet test
	```

### Deploy
- Use Docker
    1. Build Docker image
    ```
	docker-compose up
    ```
    2. Push Docker image to your production enviorment

- Use physical file
    ```
	cd DotNetCoreWebApplicationStartKit
    dotnet restore DotNetCoreWebApplicationStartKit.csproj
    dotnet build DotNetCoreWebApplicationStartKit.csproj -c Release -o /app
    ```

### Replace frontend project
- Replace folder /DotNetCoreWebApplicationStartKit/Frontend with your own project
- Replace port setting at /DotNetCoreWebApplicationStartKit/appsettings.json > AppSettings > ClientDevServerPort with your react application development server start port
- Your frontend project should build into /DotNetCoreWebApplicationStartKit/Frontend/dist folder in production mode, if not please reference Microsoft MSDN .NET Core guide to change deploy setting
