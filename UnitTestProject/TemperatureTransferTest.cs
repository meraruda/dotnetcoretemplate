using Microsoft.VisualStudio.TestTools.UnitTesting;
using DotNetCoreWebApplicationStartKit.Service;

namespace UnitTestProject
{
    [TestClass]
    public class TemperatureTransferTest
    {
        [DataTestMethod]
        [DataRow(30, 85)]
        [DataRow(32, 89)]
        public void CtoF(int temperatureC, int temperatureF)
        {
            var result = TemperatureTransfer.CtoF(temperatureC);
            Assert.AreEqual(result, temperatureF);
        }
    }
}
