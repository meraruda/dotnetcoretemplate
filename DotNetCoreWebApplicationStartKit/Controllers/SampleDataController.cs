using DotNetCoreWebApplicationStartKit.Models;
using DotNetCoreWebApplicationStartKit.Models.ApiRequest;
using DotNetCoreWebApplicationStartKit.Models.ApiResponse;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;

//Known issues
//Request examples are not shown for querystring parameters(i.e.HTTP GET requests, or for querystring parameters for POST, PUT etc methods). Request examples will only be shown in request body.This is as per the OpenApi 3.0 spec.

namespace DotNetCoreWebApplicationStartKit.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SampleDataController : ControllerBase
    {
        ILogger _logger { get; set; }

        public SampleDataController(ILogger logger) {
            _logger = logger;
            _logger.Debug("log sample");
        }

        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        /// <summary>
        /// a sample api
        /// </summary>
        [HttpPost("[action]")]
        [SwaggerRequestExample(typeof(WeatherForecast), typeof(PostWeatherForecastRequestExample))]
        [SwaggerResponse(200, "create success")]
        [SwaggerResponse(400, "BadRequest - validation fail")]
        public IActionResult WeatherForecastsPost([FromBody]WeatherForecast request)
        {
            return Ok();
        }

        /// <summary>
        /// a sample api
        /// </summary>
        [HttpGet("[action]")]
        [SwaggerResponse(200, "The list of weather", typeof(IEnumerable<WeatherForecast>))]
        [SwaggerResponseExample(200, typeof(WeatherForecastExample))]
        public IActionResult WeatherForecastsGet([FromQuery]GetWeatherForecastRequest request)
        {
            var rng = new Random();
            return Ok(Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                DateFormatted = DateTimeOffset.FromUnixTimeMilliseconds(request.date).ToString("d"),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            }));
        }

        /// <summary>
        /// a sample api
        /// </summary>
        [HttpPut("[action]")]
        [SwaggerRequestExample(typeof(WeatherForecast), typeof(PostWeatherForecastRequestExample))]
        [SwaggerResponse(200, "update success")]
        [SwaggerResponse(400, "BadRequest - validation fail")]
        public IActionResult WeatherForecastsPut([FromBody]WeatherForecast request)
        {
            return Ok();
        }

        /// <summary>
        /// a sample api
        /// </summary>
        [HttpDelete("[action]")]
        [SwaggerResponse(200, "delete success")]
        [SwaggerResponse(400, "BadRequest - validation fail")]
        public IActionResult WeatherForecastsDelete([FromQuery]GetWeatherForecastRequest request)
        {
            return Ok();
        }
    }

   
}

