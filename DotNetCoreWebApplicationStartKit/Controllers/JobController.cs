﻿using Castle.Core;
using DotNetCoreWebApplicationStartKit.Interceptor;
using DotNetCoreWebApplicationStartKit.Jobs;
using DotNetCoreWebApplicationStartKit.Models.ApiRequest;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Quartz;
using Quartz.Impl;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DotNetCoreWebApplicationStartKit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Interceptor(typeof(SampleInterceptor))]
    public class JobController : ControllerBase
    {
        private readonly IConfiguration _config;

        public JobController(IConfiguration configuration)
        {
            _config = configuration;
        }       

        /// <summary>
        /// Trigger Quartz Job
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        [SwaggerResponse(200, "success")]
        [SwaggerResponse(400, "fail")]
        public virtual async Task<IActionResult> Sample() {

            var schedulerFactory = new StdSchedulerFactory();
            var scheduler = schedulerFactory.GetScheduler().Result;

            var job = JobBuilder.Create<SampleJob>()
                .WithIdentity("localJob", "default")
                .Build();

            var trigger = TriggerBuilder.Create()
                .WithIdentity("trigger1", "default")
                .ForJob(job)
                .StartNow()
                .WithCronSchedule("0 /1 * ? * *")
                .Build();

            await scheduler.ScheduleJob(job, trigger);

            await scheduler.Start();

            return Ok();
        }
    }
}