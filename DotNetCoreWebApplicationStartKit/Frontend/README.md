# WiMM VPHTPE

## 特色

* [react 16.8](https://github.com/facebook/react)
* [webpack v4](https://github.com/webpack/webpack)
* [babel v7](https://github.com/babel/babel)
* [styled-components v4](https://github.com/styled-components/styled-components)

## Script

| `npm run <script>` | 說明                             |
|--------------------|---------------------------------|
| `start`            | 啟動網站在 8000 port              |
| `build`            | 編譯整個網站成品到 dist 資料夾下     |

## ⚙️ 設定

- [環境變數設定](docs/env_setting.md)

## Icon 設定

- [Icon設定](docs/icon_setting.md)

## Changle Log

- [Change Log Spec](docs/changelog_spec.md)