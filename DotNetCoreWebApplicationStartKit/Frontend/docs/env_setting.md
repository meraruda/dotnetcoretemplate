# 環境變數設定

## .env檔案

- 設定的環境變數會覆蓋原本電腦內的環境變數，因此可以針對專案內容設定

```
REACT_STARTKIT
└── .env
```

## 設定

```
// .env file

HOST_URL=http://xxx.xxx.com
```

## 如何使用

- 在webpack中使用，可參考 webpack/config/devServer.js

```
// 直接使用process.env.[ENV_VAR]

proxy: {
  '/web': {
    changeOrigin: true,
    target: process.env.HOST_URL,
    secure: false,
  },
},
```

- 在React中使用

```
/*
  1. 需要先在webpack中設定要給React用的環境變數，可參考 webpack/config/plugin.js
     EnvironmentPlugin中設定你要丟給React使用的變數名稱
*/

new webpack.EnvironmentPlugin(['HOST_URL'])

/*
  2. 直接使用process.env.[ENV_VAR]，可參考 src/config/app.base.config.js
*/

const config = {
  hostUrl: `${process.env.HOST_URL}/#`,
};
```