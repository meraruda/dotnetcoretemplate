# Changelog spec

## 填寫規範

### 圖示說明
- 🔥 ：Added a new component 
- 🌟 ：Added a new feature
- 💄 ：Added new props support
- 🗑 ：Remove props support
- 🐞 ：Fix Bug
- 📖 ：Added Document
- 🎉 ：Release something

### 段落說明

- 第一行爲版號，使用 ### 標記
- 第二行爲日期，使用 `<code></code>` 標記
- 第三行爲版本說明文字，使用 `>` 標記
- 第四行以後為變更項目，使用項目符號(-)做階層標記，若有issue id 則標記在後方。

### 範例：

### <b>0.0.1</b>
<code>2018-12-04</code>
> 此為版本說明文字

- 📖  第一版changelog 規範釋出
- Button
    - 🔥 新增按鈕元件 
- 🎉  加入 docenv 統一環境變數的設定方式 #8 
