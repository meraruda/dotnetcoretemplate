# Icon設定

- Icon預設使用Ant Design，但是v3.9後import icon時會將所有icon都載入
  - [Issue](https://github.com/ant-design/ant-design/issues/12011#issuecomment-420038579)
- 目前使用webpack alias修改，載入時減少500kb
- 如果需要用的Ant Design提供的svg icon的話，可以在 src/icon.ts中加入