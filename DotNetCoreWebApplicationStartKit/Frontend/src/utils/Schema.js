import * as Yup from 'yup';

export const exampleSchema = {
  name: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  age: Yup.number()
    .required('Required'),
  address: Yup.string()
    .required('Required'),
};
