// const csv is the CSV file with headers
export const csvToJson = (csv) => {
  const lines = csv.split('\n');
  const json = [];

  // 取得表頭欄位
  const headers = lines[0].split(',').map(header => header.toLowerCase().trim());
  for (let i = 1; i < lines.length; i += 1) {
    const obj = {};
    if (lines[i] !== '') {
      const currentline = lines[i].split(',');
      for (let j = 0; j < headers.length; j += 1) {
        obj[j] = currentline[j] ? currentline[j].trim() : '';
      }
      json.push(obj);
    }
  }
  return json;
};
