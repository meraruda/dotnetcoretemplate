import PATH from '../routes/PathConstants';
import CommonHelper from './CommonHelper';
import * as CSVUtils from './CSVUtils';
import * as SearchUtils from './SearchUtils';
import * as Auth from './Auth.js';

export {
  PATH, CSVUtils, SearchUtils, CommonHelper, Auth,
};

export async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index += 1) {
    await callback(array[index], index, array);
  }
}


const defaultMasssge = { key: '', variable: {} };
export const toggleNotificaiton = (masssge = defaultMasssge, callback = () => {}) => {
  const params = { ...defaultMasssge, ...masssge };
  if (masssge.key !== '') {
    global.state.notificatonStore.openNotification(params.key, params.variable, callback);
  }
};
