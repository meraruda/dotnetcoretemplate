export const LOCAL_KEYS = {
  SYNC_SESSION: 's_s',
  UPDATE_SESSION: 'u_s',
  REMOVE_SESSION: 'r_s',
  STORE_SESSION_DATA: 's_s_d',
};

export const SESSION_KEYS = {
  TOKEN: 'w_t',
};

export function isLogin() {
  // const token = sessionStorage.getItem(SESSION_KEYS.TOKEN);
  // return token !== '' && token !== 'null' && token !== null;
  return true;
}

export function getToken() {
  return sessionStorage.getItem(SESSION_KEYS.TOKEN);
}

export function deleteToken() {
  sessionStorage.removeItem(SESSION_KEYS.TOKEN);
  localStorage.setItem(LOCAL_KEYS.REMOVE_SESSION, Date.now());
}

export function setToken(token) {
  sessionStorage.setItem(SESSION_KEYS.TOKEN, token);
  localStorage.setItem(LOCAL_KEYS.UPDATE_SESSION, `${Date.now()},${token}`);
}

export default {};
