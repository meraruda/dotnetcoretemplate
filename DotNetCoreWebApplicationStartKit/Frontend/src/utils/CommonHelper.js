import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

/**
 * CommonHelper 功能說明
 * 1. 提供router相關功能
 * 2. 提供語系相關功能
 * 3. 是否顯示重新整理/關閉頁面提示訊息
 */
export default ({ refresh = false, refreshBlackList = [] } = { refresh: false, refreshBlackList: [] }) => (WrappedComponent) => {
  @withRouter
  @withTranslation()
  class CommonHelper extends PureComponent {
    componentDidMount() {
      this.onbeforeunload(true);
    }

    componentWillUnmount = () => {
      this.onbeforeunload(false);
    }

    onbeforeunload = (isActive) => {
      if (process.env.NODE_ENV === 'development' || !refresh) return;
      const { match: { path, url } } = this.props;
      const isInBlackList = refreshBlackList.filter((urlPattern => path.includes(urlPattern) || url.includes(urlPattern))).length === 0;

      // remove listener
      if (isInBlackList) {
        // 防止refresh
        if (isActive) {
          window.onbeforeunload = () => 'Data will be lost if you leave the page, are you sure?';
        } else {
          window.onbeforeunload = () => { };
        }
      }
    }

    render() {
      return (
        <WrappedComponent {...this.props} />
      );
    }
  }

  return CommonHelper;
};
