const SESSION_KEY = 'search';

function updateSearch(key, search) {
  const originSearch = JSON.parse(window.sessionStorage.getItem(SESSION_KEY));
  const newVal = {
    ...JSON.parse(originSearch[key]),
    ...search,
  };
  originSearch[key] = JSON.stringify(newVal);
  window.sessionStorage.setItem(SESSION_KEY, JSON.stringify(originSearch));
  return originSearch;
}

export function initSearch(key, { sorter, pagination, query, ...rests }) {
  const originSearch = JSON.parse(window.sessionStorage.getItem(SESSION_KEY) || '{}');
  // if (!originSearch.hasOwnProperty(key)) {
  originSearch[key] = JSON.stringify({
    ...rests, sorter, pagination, query,
  });
  window.sessionStorage.setItem(SESSION_KEY, JSON.stringify(originSearch));
  // }
  return originSearch;
}

export function getSearch(key) {
  const search = JSON.parse(window.sessionStorage.getItem(SESSION_KEY)) || '{}';
  const data = JSON.parse(search[key]);
  return {
    pagination: data.pagination,
    sorter: data.sorter,
    query: data.query,
  };
}

export function updateQuery(key, query) {
  const originSearch = JSON.parse(window.sessionStorage.getItem(SESSION_KEY));
  return updateSearch(key, {
    query,
    pagination: {
      ...JSON.parse(originSearch[key]).pagination,
      current: 1,
    },
  });
}

export function updatePagination(key, pagination) {
  return updateSearch(key, { pagination });
}

export function updateSorter(key, sorter) {
  const originSearch = JSON.parse(window.sessionStorage.getItem(SESSION_KEY));
  return updateSearch(key, {
    sorter,
    pagination: {
      ...JSON.parse(originSearch[key]).pagination,
      current: 1,
    },
  });
}

export async function resetSearch() {
  window.sessionStorage.removeItem(SESSION_KEY);
}

export function genPagination(pageSize = 10, current = 1, totalNumber = 0) {
  return {
    current,
    pageSize,
    totalNumber,
  };
}

export function genSorter(field = 'update_time', type = 'desc') {
  return {
    field,
    type,
  };
}
