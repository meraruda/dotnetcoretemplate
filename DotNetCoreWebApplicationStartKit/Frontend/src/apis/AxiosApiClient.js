import axios, { CancelToken } from 'axios';
import AppConfig from '~~config';
import { deleteToken, isLogin, getToken } from '~~utils/Auth';
// import { toggleNotificaiton, } from '~~utils/';

import { resetUser } from '~~modules/User/UserAction';
import { clearMenu } from '~~modules/App/AppActions';
// import httpAdapter from 'axios/lib/adapters/http';

const API_CONFIG = {
  baseURL: AppConfig.apiDomain,
  timeout: 120000,
};

class AxiosApiClient {
  constructor(apiConfig) {
    this.axiosInstance = axios.create({ ...API_CONFIG, ...apiConfig });
    // this.setHeader();
  }

  setHeader(headerObject = null) {
    const Authorization = isLogin() ? { Authorization: getToken() } : {};

    const defaultHeaders = {
      ...Authorization,
    };
    this.axiosInstance.defaults.headers = { ...defaultHeaders, ...headerObject };
  }

  instance() {
    return this.axiosInstance;
  }

  get(url, payload, logType = '') {
    this.setHeader();
    if (logType.length !== 0) {
      this.log(logType);
    }
    return this.axiosInstance.get(...[url, payload]).catch(this.onError);
  }

  cancelableGet(url, payload, config) {
    this.setHeader(config.header);
    const source = CancelToken.source();
    return {
      send: () => this.axiosInstance
        .get(url, { ...payload, cancelToken: source.token }),
      cancel: source.cancel,
    };
  }

  post(url, payload, logType = '', header = {}) {
    this.setHeader(header);
    if (logType.length !== 0) {
      this.log(logType);
    }
    return this.axiosInstance.post(...[url, payload]).catch(this.onError);
  }

  cancelablePost(url, payload, config) {
    this.setHeader(config.header);
    const source = CancelToken.source();
    return {
      send: () => this.axiosInstance
        .post(url, payload, { ...config, cancelToken: source.token }),
      cancel: source.cancel,
    };
  }

  delete(url, payload, logType = '', header = {}) {
    this.setHeader(header);
    if (logType.length !== 0) {
      this.log(logType);
    }
    return this.axiosInstance.delete(...[url, payload]).catch(this.onError);
  }

  cancelableDelete(url, payload, config) {
    this.setHeader(config.header);
    const source = CancelToken.source();
    return {
      send: () => this.axiosInstance
        .delete(url, payload, { ...config, cancelToken: source.token }),
      cancel: source.cancel,
    };
  }

  put(url, payload, logType = '', header = {}) {
    this.setHeader(header);
    if (logType.length !== 0) {
      this.log(logType);
    }
    return this.axiosInstance.put(...[url, payload]).catch(this.onError);
  }

  cancelablePut(url, payload, config) {
    this.setHeader(config.header);
    const source = CancelToken.source();
    return {
      send: () => this.axiosInstance
        .put(url, payload, { ...config, cancelToken: source.token }),
      cancel: source.cancel,
    };
  }

  patch(...args) {
    this.setHeader();
    return this.axiosInstance.patch(...args).catch(this.onError);
  }

  log(logType) {
    console.info(logType);
    this.post('log/addUserOperating', { user_event: logType });
  }

  onError(error) {
    if (axios.isCancel(error)) {
      console.warn('Request canceled by user...');
    } else {
      let statudCode = 500;
      let errorKey = 'SYSTEM_ERROR';
      let errorData = {};
      if (error.response) {
        statudCode = error.response.status;
        errorKey = (error.response.data && error.response.data.message)
          ? error.response.data.message
          : errorKey;
        errorData = (error.response.data && error.response.data.data)
          ? error.response.data.data
          : errorData;
      } else if (error.request) {
        statudCode = error.request.status;
      }

      const errorObj = {
        status: statudCode,
        errorKey,
        errorData,
      };

      if (statudCode === 401) {
        toggleNotificaiton({ key: 'API_ERROR.TOKEN_EXPIRED' }, () => {
          // 強制登出
          deleteToken();
          resetUser();
          clearMenu();
          window.location.href = '/';
        });
        return;
      }

      if (statudCode === 403) {
        toggleNotificaiton({ key: 'API_ERROR.FORBIDDEN' }, () => {
          /**
           * @todo
           * return to user profile page
           */
          // window.location.href = `${AppConfig.hostUrl}/user/profile`;
          // console.log('1111');
        });
        return;
      }

      if (errorKey === 'SYSTEM_ERROR') {
        toggleNotificaiton({
          key: 'API_ERROR.SYSTEM_ERROR',
          variable: { status_code: statudCode },
        });
        throw new Error(JSON.stringify({
          status: statudCode,
          errorKey: '',
        }));
      }

      throw new Error(JSON.stringify(errorObj));
    }
  }
}

export default AxiosApiClient;
