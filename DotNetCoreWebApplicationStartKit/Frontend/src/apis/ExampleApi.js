export function fakeApi(payload) {
  console.log('payload', payload);

  const fakeData = [{
    key: '1',
    name: 'John Brown',
    age: 32,
    address: 'New York No. 1 Lake Park',
    tags: ['nice', 'developer'],
  }, {
    key: '2',
    name: 'Jim Green',
    age: 42,
    address: 'London No. 1 Lake Park',
    tags: ['loser'],
  }, {
    key: '3',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park',
    tags: ['cool', 'teacher'],
  }];

  const data = fakeData.filter(o => o.name.toLocaleLowerCase().indexOf(payload.query.name.toLocaleLowerCase()) !== -1);

  return new Promise((resolve) => {
    setTimeout(() => {
      resolve({
        data,
        pageResult: {
          ...payload.pagination,
          totalNumber: data.length,
        },
      });
    }, 1000);
  });
}
