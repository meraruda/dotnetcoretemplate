import React, { Suspense, lazy } from 'react';
import { Switch, Route } from 'react-router-dom';
import PATH, { APP_PREFIX, FULLPAGE_PREFIX } from './PathConstants';

const EmptyPage = () => ('Opps... Something went wrong. Please check your url path again.');

export const WaitingComponent = Component => props => (
  <Suspense fallback={<div>Module loading....</div>}>
    <Component {...props} />
  </Suspense>
);

/*
  Layout
*/
const App = lazy(() => import('~~modules/App/App/App.js'));
const FullPage = lazy(() => import('~~modules/App/FullPage/FullPage'));

const routes = () => (
  <Switch>
    <Route path={APP_PREFIX} render={WaitingComponent(App)} />
    <Route path={FULLPAGE_PREFIX} render={WaitingComponent(FullPage)} />
    <Route path={null} render={EmptyPage} />
  </Switch>
);

/*
  APP Routes
*/
const Example = lazy(() => import('~~modules/Example/Example.js'));
const HookExample = lazy(() => import('~~modules/HookExample/HookExample.js'));
const Demo = lazy(() => import('~~modules/Demo/Demo'));
const Store = lazy(() => import('~~modules/Store/Store'));

const appRoutes = [
  { path: PATH.HOOK_EXAMPLE, render: WaitingComponent(HookExample) },
  { path: PATH.EXAMPLE, render: WaitingComponent(Example) },
  { path: PATH.DEMO, render: WaitingComponent(Demo) },
  { path: PATH.STORE, render: WaitingComponent(Store) },
  { path: null, render: EmptyPage },
];

export const AppRoutes = () => (
  <Switch>
    {appRoutes.map(config => <Route exact {...config} />)}
  </Switch>
);

/*
  FULLPAGE Routes
*/
const Login = lazy(() => import('~~modules/NoLogin/Login/Login'));

const fullpageRoutes = [
  { path: PATH.LOGIN, render: WaitingComponent(Login) },
  { path: null, render: EmptyPage },
];

export const FullpageRoutes = () => (
  <Switch>
    {fullpageRoutes.map(config => <Route exact {...config} />)}
  </Switch>
);

export default routes;
