import { Component } from 'react';
// import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
// import { Consumer } from '~~store/';
// import { LOCAL_KEYS, SESSION_KEYS, isLogin } from '~~utils/Auth';
// import { PATH } from '~~utils/';

// const NO_LOGIN_PAGE = [
//   '/login',
//   '/forgetPassword/:uuid',
// ];

// const propTypes = {
//   children: PropTypes.node.isRequired,
// };

@withRouter
// @Consumer('userStore')
class Authentication extends Component {
  // static propTypes = propTypes;

  componentWillMount() {
    // const { context: { userStore }, history } = this.props;

    // if (!userStore.isLogin) {
    //   history.push(PATH.LOGIN);
    // }

    // localStorage.setItem(LOCAL_KEYS.SYNC_SESSION, Date.now());

    // this.checkLogin(this.props);

    // window.onstorage = (e) => {
    //   if (!e.newValue) return;

    //   const { history } = this.props;
    //   // GET_SESSION
    //   // 從其他 tab 取得資料放到 localStorage，再刪除
    //   if (e.key === LOCAL_KEYS.SYNC_SESSION) {
    //     localStorage.setItem(LOCAL_KEYS.STORE_SESSION_DATA, sessionStorage.getItem(SESSION_KEYS.TOKEN));
    //     localStorage.removeItem(LOCAL_KEYS.STORE_SESSION_DATA);
    //     this.checkLogin(this.props);
    //   }

    //   // STORE_SESSION_DATA
    //   // 監聽 localStorage 被寫入，寫到 sessionStorage
    //   if (e.key === LOCAL_KEYS.STORE_SESSION_DATA && !!e.newValue) {
    //     sessionStorage.setItem(SESSION_KEYS.TOKEN, e.newValue);
    //     localStorage.removeItem(LOCAL_KEYS.SYNC_SESSION);
    //     this.checkLogin(this.props);
    //   }

    //   // UPDATE_SESSION
    //   // 更換TOKEN (change workspace / login)
    //   if (e.key === LOCAL_KEYS.UPDATE_SESSION && !!e.newValue) {
    //     const token = e.newValue.split(',')[1];
    //     sessionStorage.setItem(SESSION_KEYS.TOKEN, token);
    //     localStorage.removeItem(LOCAL_KEYS.UPDATE_SESSION);

    //     if (isLogin()) {
    //       // 切換workspace
    //       window.location.reload();
    //     } else {
    //       // 登入
    //       history.push(PATH.USER_PROFILE);
    //     }
    //   }
    //   // REMOVE_SESSION
    //   // 監聽其他 tab 登出
    //   if (e.key === LOCAL_KEYS.REMOVE_SESSION) {
    //     sessionStorage.removeItem(SESSION_KEYS.TOKEN);
    //     localStorage.removeItem(LOCAL_KEYS.REMOVE_SESSION);
    //     history.push(PATH.LOGIN);
    //   }
    // };
  }

  // componentWillUpdate(nextProps) {
  //   this.checkLogin(nextProps);
  // }

  // checkLogin = (props) => {
  //   const { history } = this.props;
  //   if (!isLogin() && !NO_LOGIN_PAGE.find(url => url === props.match.path)) {
  //     // 沒有登入，且網址不在免登入白名單之內，將網址導至登入頁
  //     sessionStorage.removeItem(SESSION_KEYS.TOKEN);
  //     history.push(PATH.LOGIN);
  //   } else if (isLogin() && (props.location.pathname === PATH.LOGIN || props.location.pathname === '/')) {
  //     // 有登入，但是還在登入頁，轉導到系統首頁
  //     history.push(PATH.USER_PROFILE);
  //   }
  // }

  render() {
    const { children } = this.props;
    return children;
  }
}

export default Authentication;
