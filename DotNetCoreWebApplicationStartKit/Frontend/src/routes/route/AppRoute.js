import React from 'react';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import App from '~~modules/App/App/App';
import FullPage from '~~modules/App/FullPage/FullPage';
import ScrollToTop from './ScrollToTop';
import Authentication from './Authentication';

const AppRoute = ({ component: Component, pageType, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      <Authentication>
        <ScrollToTop>
          {
            pageType === 'FULL' && (
              <FullPage>
                <Component {...props} />
              </FullPage>
            )
          }
          {
            (!pageType || pageType === 'APP') && (
              <App>
                <Component {...props} />
              </App>
            )
          }
        </ScrollToTop>
      </Authentication>
    )}
  />
);

AppRoute.propTypes = {
  component: PropTypes.func.isRequired,
  pageType: PropTypes.oneOf(['FULL', 'APP']),
};

AppRoute.defaultProps = {
  pageType: 'APP',
};

export default AppRoute;
