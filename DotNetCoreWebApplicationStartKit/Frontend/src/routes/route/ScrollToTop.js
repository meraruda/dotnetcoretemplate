import { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';


const propTypes = {
  location: PropTypes.shape({
    hash: PropTypes.string,
    key: PropTypes.string,
    pathname: PropTypes.string,
    search: PropTypes.string,
    state: PropTypes.string,
  }).isRequired,
  children: PropTypes.node.isRequired,
};


class ScrollToTop extends Component {
  static propTypes = propTypes;

  componentWillMount() {
    if (window.history.scrollRestoration) {
      window.history.scrollRestoration = 'manual';
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0);
    }
  }

  render() {
    return this.props.children;
  }
}

export default withRouter(ScrollToTop);
