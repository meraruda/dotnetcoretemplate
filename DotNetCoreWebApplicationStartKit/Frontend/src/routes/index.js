import Routes, { AppRoutes, FullpageRoutes } from './routes';

export default Routes;
export { AppRoutes, FullpageRoutes };
