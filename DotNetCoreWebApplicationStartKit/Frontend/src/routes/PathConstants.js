export const APP_PREFIX = '/app';
export const FULLPAGE_PREFIX = '/';

export default {
  LOGIN: '/',

  EXAMPLE: `${APP_PREFIX}/example`,
  STORE: `${APP_PREFIX}/store`,
  DEMO: `${APP_PREFIX}/demo`,
  HOOK_EXAMPLE: `${APP_PREFIX}/hook`,
};
