import { createGlobalStyle } from 'styled-components';
import ant_variables from '~~styles';

export default {
  primary_brand: '#383838',
  darker_brand: '#808080',

  bg_full_page: '#3c3c3c',
  bg_app: '#f0f0f0',
  bg_header: '#808080',
  bg_footer: '#ed5b00',

  bg_darkergray: '#3c3c3c',
  light_gray: '#888888',

  'menu-hover-bg-color': '#787271',
};

export const injectGlobalStyle = () => createGlobalStyle`
  body {
    font-family: "PingFang SC", 'Microsoft JhengHei', sans-serif;
    font-size: 14px;
    background-color: #333;

    .ant-dropdown-menu-item-active {
      color: ${ant_variables['@table-header-color']};
    }
  }
`;
