// export what antd other components need
export {
  default as DownOutline
} from '@ant-design/icons/lib/outline/DownOutline';

export {
  default as LeftOutline
} from '@ant-design/icons/lib/outline/LeftOutline';

export {
  default as RightOutline
} from '@ant-design/icons/lib/outline/RightOutline';

export {
  default as DoubleRightOutline
} from '@ant-design/icons/lib/outline/DoubleRightOutline';

export {
  default as CaretUpFill
} from '@ant-design/icons/lib/fill/CaretUpFill';

export {
  default as CaretDownFill
} from '@ant-design/icons/lib/fill/CaretDownFill';

export {
  default as FilterFill
} from '@ant-design/icons/lib/fill/FilterFill';

export {
  default as WarningOutline
} from '@ant-design/icons/lib/outline/WarningOutline';

export {
  default as CloseCircleOutline
} from '@ant-design/icons/lib/outline/CloseCircleOutline';

export {
  default as QuestionCircleOutline
} from '@ant-design/icons/lib/outline/QuestionCircleOutline';

export {
  default as FilterOutline
} from '@ant-design/icons/lib/outline/FilterOutline';

export {
  default as Setting
} from '@ant-design/icons/lib/outline/SettingOutline';

export {
  default as CloseOutline
} from '@ant-design/icons/lib/outline/CloseOutline';
