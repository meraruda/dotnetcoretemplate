import React from 'react';
import PropTypes from 'prop-types';

import * as Style from './Style';

const Main = (props) => {
  const { children, title } = props;

  return (
    <Style.Main>
      <Style.Title>
        { title }
      </Style.Title>
      { children }
    </Style.Main>
  );
};


Main.propTypes = {
  /** 各頁面 Title */
  title: PropTypes.string,
};

Main.defaultProps = {
  title: '',
};

export default Main;
