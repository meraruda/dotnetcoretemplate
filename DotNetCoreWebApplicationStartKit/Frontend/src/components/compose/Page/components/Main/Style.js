import styled from 'styled-components';

export const Main = styled.div`
  width: 100%;
`;

export const Title = styled.div`
  font-size: 20px;
  margin-bottom: 10px;
`;
