import React from 'react';
import PropTypes from 'prop-types';

import * as Style from './Style';

const ALIGN = {
  CENTER: 'center',
  LEFT: 'left',
  RIGHT: 'right',
};

const Column = (props) => {
  const { align, children } = props;

  return (
    <Style.Column align={align}>
      { children }
    </Style.Column>
  );
};

Column.propTypes = {
  /** position of content (center, left, right) */
  align: PropTypes.string,
};

Column.defaultProps = {
  align: ALIGN.LEFT,
};

export default Column;
