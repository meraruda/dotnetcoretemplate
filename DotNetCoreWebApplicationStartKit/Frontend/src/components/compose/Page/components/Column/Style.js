import styled from 'styled-components';

export const Column = styled.div`
  display: flex;
  align-items: center;
  ${(p) => {
    switch (p.align) {
      case 'center':
        return 'justify-content: center;';
      case 'left':
        return 'justify-content: flex-start;';
      case 'right':
        return 'justify-content: flex-end;';
      default:
        return 'justify-content: center;';
    }
  }}
  flex: 1;
  text-align: ${props => props.align};
`;
