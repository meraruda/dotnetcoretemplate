import React from 'react';

import * as Style from './Style';

const Row = (props) => {
  const { children } = props;

  return (
    <Style.Row>
      { children }
    </Style.Row>
  );
};

export default Row;
