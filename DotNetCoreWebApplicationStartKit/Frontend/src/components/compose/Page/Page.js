import Main from './components/Main/Main';
import Row from './components/Row/Row';
import Column from './components/Column/Column';

export default {
  Main,
  Row,
  Column,
};
