import React, { Component } from 'react';
import PropTypes from 'prop-types';
import produce from 'immer';
import isEqual from 'lodash/isEqual';

export default class Main extends Component {
  static getDerivedStateFromProps(props, state) {
    if (!isEqual(props.query, state.INIT_STATE)) {
      return {
        query: props.query,
        INIT_STATE: props.query,
      };
    }
  }

  state = {
    query: this.props.query,
    INIT_STATE: this.props.query,
  }

  componentDidMount() {
    this.onSearch();
  }

  changeQuery = async (key, value) => {
    this.setState(state => produce(state, (draftState) => {
      draftState.query[key] = value;
    }));
  }

  onSearch = () => {
    const { handleSearch } = this.props;
    handleSearch(this.state.query);
  }

  onReset = () => {
    const { INIT_STATE } = this.state;

    this.setState({
      query: INIT_STATE,
    });
  }

  render() {
    return (
      <div>
        {this.props.children({
          state: this.state.query,
          changeQuery: this.changeQuery,
          onSearch: this.onSearch,
          onReset: this.onReset,
        })}
        {/* {React.Children.map(child => child({ value: 111 }))} */}
      </div>
    );
  }
}

Main.propTypes = {
  /** position of content (center, left, right) */
  query: PropTypes.shape(),
};

Main.defaultProps = {
  query: {},
};
