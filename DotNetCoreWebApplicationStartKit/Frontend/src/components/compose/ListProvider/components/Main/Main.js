import React, { Component, createContext } from 'react';
import dayjs from 'dayjs';
import { SearchUtils, toggleNotificaiton } from '~~utils/';

export const ListContext = createContext();

export default class Main extends Component {
  static getDerivedStateFromProps(props, state) {
    return {
      query: Object.assign({}, state.query, props.query),
    };
  }

  constructor(p) {
    super(p);
    this.search_key = this.props.searchKey;
    SearchUtils.initSearch(this.search_key, {
      pagination: SearchUtils.genPagination(this.props.pageSize || 10),
      sorter: SearchUtils.genSorter(this.props.sortField),
      query: {},
    });
  }

  handleSearch = async (query) => {
    SearchUtils.updateQuery(this.search_key, query);
    await this.getList();
  }

  onSortAndPageChange = async (sorter, pagination) => {
    if (Object.keys(sorter).length > 0) {
      SearchUtils.updateSorter(this.search_key, { field: sorter.field, type: sorter.order === 'ascend' ? 'asc' : 'desc' });
    }
    if (Object.keys(pagination).length > 0) {
      SearchUtils.updatePagination(this.search_key, pagination);
    }

    await this.getList();
  }

  getList = async () => {
    await this.setState(() => ({ isLoading: true }));
    try {
      const payload = SearchUtils.getSearch(this.search_key);
      const { data, pageResult } = await this.props.getListFunction(payload);
      SearchUtils.updatePagination(this.search_key, pageResult);
      this.setState({
        data,
        query: payload.query,
        listUpdateTime: dayjs().format('YYYY/MM/DD HH:mm:ss'),
      });
    } catch (err) {
      const error = JSON.parse(err.message);
      toggleNotificaiton({ key: error.errorKey });
    } finally {
      await this.setState(() => ({ isLoading: false }));
    }
  }

  getSearchObj = () => SearchUtils.getSearch(this.search_key)

  getListProps = () => ({
    state: { ...this.state },
    handleSearch: this.handleSearch,
    getList: this.getList,
    getSearchObj: this.getSearchObj,
  })

  state = {
    data: [],
    isLoading: false,
    listUpdateTime: '---',
    handleSearch: this.handleSearch,
    getSearchObj: this.getSearchObj,
    onSortAndPageChange: this.onSortAndPageChange,
  }

  render() {
    return (
      <div>
        <ListContext.Provider value={this.state}>
          {this.props.children(this)}
          {/* {React.Children.map(child => child({ value: 111 }))} */}
        </ListContext.Provider>
      </div>
    );
  }
}
