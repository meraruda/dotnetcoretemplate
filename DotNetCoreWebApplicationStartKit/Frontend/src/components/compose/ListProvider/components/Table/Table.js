import React, { Component } from 'react';
import { Table } from '~~atoms';
import { ListContext } from '../Main/Main';

export default class TableComp extends Component {
  static contextType = ListContext;

  onChangeTable = (pagination, filters, sorter) => {
    const { onSortAndPageChange } = this.context;

    onSortAndPageChange(sorter, pagination);
  }

  render() {
    const { data, listUpdateTime, getSearchObj, isLoading } = this.context;

    return (
      <Table
        dataSource={data}
        loading={isLoading}
        pagination={getSearchObj().pagination}
        title={() => `總共 ${getSearchObj().pagination.totalNumber} 筆`}
        footer={() => `取得時間：${listUpdateTime}`}
        onChange={this.onChangeTable}
        {...this.props}
      />
    );
  }
}
