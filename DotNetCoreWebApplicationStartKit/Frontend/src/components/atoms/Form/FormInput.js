import React, { Component } from 'react';
import styled from 'styled-components';
import { Input } from '~~atoms';
import { FormContext } from './Main';

const Container = styled.div`
  margin-bottom: 10px;

  .errorMessage {
    color: red;
  }
`;

class FormInput extends Component {
  static contextType = FormContext;

  getProps = () => {
    const { name } = this.props;
    const { setFieldValue, onChange, data } = this.context;

    return {
      ...this.props,
      value: data[name],
      handleChange: (value) => {
        onChange(name, value);
        setFieldValue(name, value);
      },
    };
  }

  render() {
    const { name } = this.props;
    const { errors, touched } = this.context;

    const error = (errors[name] && touched[name]) ? errors[name] : '';

    return (
      <Container>
        <Input
          {...this.getProps()}
        />
        {error && (
          <div className="errorMessage">{error}</div>
        )}
      </Container>
    );
  }
}

export default FormInput;
