import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Icon } from '@ant-design/compatible';

const BUTTON_SIZE = {
  xs: '14px',
  sm: '16px',
  md: '20px',
  lg: '22px',
};

const IconComp = styled(Icon)`
  svg {
    width: ${p => BUTTON_SIZE[p.size]};
    height: ${p => BUTTON_SIZE[p.size]};
    cursor: pointer;
  }
`;

IconComp.PropTypes = {
  /** 可輸入 xs, sm, md, lg, xl 或是 實際大小(例如 12px, 30%) */
  size: PropTypes.oneOf(['xs', 'sm', 'md', 'lg']),
};

IconComp.defaultProps = {
  size: 'md',
};

export default IconComp;
