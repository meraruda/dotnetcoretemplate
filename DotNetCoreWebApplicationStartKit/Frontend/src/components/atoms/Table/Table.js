import React, { Component } from 'react';
import styled from 'styled-components';
import { Table, Checkbox, Popover } from 'antd';
import { Icon } from '~~atoms';
import ant_variables from '~~styles';
import * as Style from './Style';

const CheckboxGroup = Checkbox.Group;

const TableStyle = styled(Table)`
  width: 100%;

  .ant-table-row {
    transition: color 0s;

    &:hover {
      color: ${ant_variables['@table-header-color']};
    }
  }

  .ant-table-footer {
    color: ${ant_variables['@table-header-color']};
  }
`;

export default class TableComp extends Component {
  state = {
    checkedValue: this.props.columns.map(c => c.key),
  }

  renderTitle = () => {
    const { title, columns } = this.props;
    const options = columns.map(c => ({ label: c.title, value: c.key }));

    const content = (
      <CheckboxGroup style={{ display: 'flex', flexDirection: 'column' }} options={options} value={this.state.checkedValue} onChange={this.handleSelected} />
    );

    return (
      <Style.Title>
        {title && title()}
        <div>
          <Popover content={content} placement="left">
            <Icon type="filter" />
          </Popover>
        </div>
      </Style.Title>
    );
  }

  handleSelected = (checkedValue) => {
    this.setState({
      checkedValue,
    });
  }

  render() {
    const { checkedValue } = this.state;
    const { pagination, columns } = this.props;
    const props = {
      bordered: true,
      pagination: {
        showSizeChanger: true,
        showQuickJumper: true,
      },
      title: this.renderTitle,
      columns: columns.filter(c => checkedValue.indexOf(c.key) !== -1),
    };

    if (pagination) {
      props.pagination = {
        ...props.pagination,
        ...pagination,
      };
    }

    return (
      <TableStyle
        {...this.props}
        {...props}
      />
    );
  }
}
