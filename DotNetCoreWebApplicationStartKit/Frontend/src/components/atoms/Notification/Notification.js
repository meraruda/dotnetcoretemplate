import React, { PureComponent } from 'react';
import { message } from 'antd';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';


@withTranslation()
class Notification extends PureComponent {
  isMount = false;

  componentDidMount() {
    this.isMount = true;
  }

  componentDidUpdate(prveProps) {
    const { isOpen, t, message: errMessage, messageVariable } = this.props;

    if (prveProps.isOpen !== isOpen) {
      if (isOpen) {
        const CLOSE_TIME = 1.5;

        message.error(t(errMessage, { status_code: messageVariable.status_code }), CLOSE_TIME);
        setTimeout(() => {
          global.state.notificatonStore.closeNotification();
        }, CLOSE_TIME * 1000);
      }
    }
  }

  render() {
    return (
      <div />
    );
  }
}

Notification.propTypes = {
  closeNotification: PropTypes.func,
};

Notification.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  message: PropTypes.string,
  messageVariable: PropTypes.shape({}),
  callback: PropTypes.func,
};

Notification.defaultProps = {
  message: '',
  messageVariable: {},
  closeNotification: () => {},
  callback: () => {},
};

export default Notification;
