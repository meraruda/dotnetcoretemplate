import 'normalize.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import { createHashHistory as createHistory } from 'history';
import { I18nextProvider } from 'react-i18next';
import { hot } from 'react-hot-loader';
import { ThemeProvider } from 'styled-components';
import Provider from '~~store/';

import Routes from './routes';
import theme, { injectGlobalStyle } from './theme';
import { UserContextProvider } from './store/user2Store';

import i18n from './locales/i18n';


const GlobalStyle = injectGlobalStyle();
const history = createHistory();

const App = () => (
  <ThemeProvider theme={theme}>
    <I18nextProvider i18n={i18n}>
      <Router history={history}>
        <Provider>
          <UserContextProvider>
            <input style={{ display: 'none' }} type="text" name="fakeusernameremembered" />
            <input style={{ display: 'none' }} type="password" name="fakepasswordremembered" />
            <GlobalStyle />
            <Routes />
          </UserContextProvider>
        </Provider>
      </Router>
    </I18nextProvider>
  </ThemeProvider>
);

ReactDOM.render(<App />, document.querySelector('#app'));

hot(module)(App);

// if (process.env.NODE_ENV !== 'production') {
//   const { whyDidYouUpdate } = require('why-did-you-update');
//   whyDidYouUpdate(React);
// }
