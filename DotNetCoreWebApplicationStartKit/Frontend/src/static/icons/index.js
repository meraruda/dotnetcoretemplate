import iconHome from './icon_home.svg';
import iconLogOut from './icon_log_out.svg';

export {
  iconHome,
  iconLogOut,
};
