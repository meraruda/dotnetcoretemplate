import React, { Component } from 'react';
import { CommonHelper } from '~~utils/';
import { Consumer } from '~~store/';
import { Button } from '~~atoms';

@CommonHelper()
@Consumer('countStore', 'loaderStore')
export default class Example extends Component {
  startLoading = () => {
    const {
      loaderStore,
    } = this.props.context;

    loaderStore.openLoader();

    setTimeout(() => {
      loaderStore.closeLoader();
    }, 2000);
  }

  render() {
    const {
      countStore: { count, increment, decrement },
    } = this.props.context;

    return (
      <div>
        Count: {count}
        <div>
          <Button
            style={{ marginRight: '10px' }}
            onClick={increment}
          >
            +
          </Button>
          <Button onClick={decrement}>-</Button>
        </div>

        <p>Loader</p>
        <div>
          <Button onClick={this.startLoading}>Start Loading</Button>
        </div>
      </div>
    );
  }
}
