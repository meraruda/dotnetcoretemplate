import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
// import { Formik } from 'formik';

import { Page } from '~~compose/';
import { useQuery, useModal, useFetch } from '~~hooks/';
import { Table, Input, Button, Modal } from '~~atoms/';

import * as ExampleApi from '~~apis/ExampleApi';
import * as Style from './Style';

const tableHeader = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    sorter: true,
    align: 'center',
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age',
    align: 'center',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
    align: 'center',
  },
  {
    title: 'Tags',
    key: 'tags',
    dataIndex: 'tags',
    align: 'center',
    render: tags => (
      <span>
        {tags.join(', ')}
      </span>
    ),
  },
];

const HookExample = () => {
  const { t } = useTranslation();
  const modal1 = useModal();
  const modal2 = useModal();
  const addModal = useModal();

  const nameQuery = useQuery({
    name: '',
  });

  const fetchFakeData = useFetch(() => ExampleApi.fakeApi(nameQuery.payload), []);

  useEffect(() => {
    console.log('didMount/ didUpdate');
    fetchFakeData.fetchData();
    return () => {
      console.log('unmount');
    };
  }, []);

  const handleSearch = () => {
    console.log('payload', nameQuery.payload);
    fetchFakeData.fetchData(nameQuery.payload);
  };

  return (
    <Page.Main title={t('COMMON.EXAMPLE_TEXT')}>
      <Page.Row>
        <Page.Column>
          <Style.SearchBar>
            <Input
              value={nameQuery.query.name}
              placeholder="請輸入姓名"
              onChange={e => nameQuery.setQuery({ name: e.target.value })}
            />
            <Button onClick={nameQuery.resetQuery}>Reset</Button>
            <Button onClick={handleSearch}>Search</Button>
            <Button onClick={() => modal1.openModal('data1')}>modal1</Button>
            <Button onClick={() => modal2.openModal('data2')}>modal2</Button>
          </Style.SearchBar>
        </Page.Column>
        <Page.Column align="right">
          <Button onClick={addModal}>新增</Button>
        </Page.Column>
      </Page.Row>
      <Page.Row>
        <Table
          columns={tableHeader}
          dataSource={fetchFakeData.data.data}
          loading={fetchFakeData.isLoading}
        />
      </Page.Row>

      <Modal
        title="modal1"
        visible={modal1.visible}
        onOk={modal1.closeModal}
        onCancel={modal1.closeModal}
      >
        modal1
      </Modal>
      <Modal
        centered
        title="modal2"
        visible={modal2.visible}
        onOk={modal2.closeModal}
        onCancel={modal2.closeModal}
      >
        modal2
      </Modal>

      {/* <Formik
        initialValues={modal}
        validationSchema={SignupSchema}
        onSubmit={handleAdd}
      >
        {({ submitForm, ...restProps }) => (
          <Modal
            title="新增"
            visible={modal.visible}
            onOk={submitForm}
            onCancel={initModal}
          >
            <Form.Main data={modal} onChange={onChangeModal} {...restProps}>
              <Form.FormInput placeholder="姓名" name="name" />
              <Form.FormInput placeholder="年齡" name="age" />
              <Form.FormInput placeholder="地址" name="address" />
            </Form.Main>
          </Modal>
        )}
      </Formik> */}
    </Page.Main>
  );
};

export default HookExample;
