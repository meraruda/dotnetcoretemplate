import React, { Component } from 'react';
import { Divider } from 'antd';

import { CommonHelper } from '~~utils/';
import { Page } from '~~compose/';
import {
  Button,
  Menu,
  Select,
} from '~~atoms';


import * as Style from './DemoStyle';

const menu = (
  <Menu>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://ant.design/components/dropdown/">1st menu item</a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://ant.design/components/dropdown/">2nd menu item</a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="https://ant.design/components/dropdown/">3rd menu item</a>
    </Menu.Item>
  </Menu>
);

@CommonHelper()
class Demo extends Component {
  render() {
    return (
      <Page.Main title="This is Component Demo">
        <Page.Row><h1>Button</h1></Page.Row>
        <Page.Row>
          <Page.Column>
            <Style.DemoButton type="primary">Primary</Style.DemoButton>
            <Style.DemoButton>Default</Style.DemoButton>
            <Style.DemoButton type="dashed">Dashed</Style.DemoButton>
            <Style.DemoButton type="danger">Danger</Style.DemoButton>
            <Style.DemoButton type="primary" ghost>Primary</Style.DemoButton>
            <Style.DemoButton ghost>Default</Style.DemoButton>
            <Style.DemoButton type="dashed" ghost>Dashed</Style.DemoButton>
            <Style.DemoButton type="danger" ghost>danger</Style.DemoButton>
          </Page.Column>
        </Page.Row>

        <Divider />

        <Page.Row><h1>Dropdrown</h1></Page.Row>
        <Page.Row>
          <Page.Column>
            <Style.DemoDropdown overlay={menu}>
              <a className="ant-dropdown-link" href="#">
                Hover me
              </a>
            </Style.DemoDropdown>
            <Style.DemoDropdown overlay={menu} placement="bottomLeft">
              <Button>bottomLeft</Button>
            </Style.DemoDropdown>
          </Page.Column>
        </Page.Row>

        <Divider />

        <Page.Row><h1>Select</h1></Page.Row>
        <Page.Row>
          <Page.Column>
            <Select defaultValue="lucy" style={{ width: 120 }}>
              <Select.Option value="jack">Jack</Select.Option>
              <Select.Option value="lucy">Lucy</Select.Option>
              <Select.Option value="disabled" disabled>Disabled</Select.Option>
              <Select.Option value="Yiminghe">yiminghe</Select.Option>
            </Select>
          </Page.Column>
        </Page.Row>

        <Divider />
      </Page.Main>
    );
  }
}

Demo.propTypes = {};

Demo.defaultProps = {};

export default Demo;
