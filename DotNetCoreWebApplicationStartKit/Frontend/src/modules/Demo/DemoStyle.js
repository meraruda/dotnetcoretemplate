import styled from 'styled-components';

import { Button, Dropdown } from '~~atoms';

export const DemoButton = styled(Button)`
  margin-right: 10px;
`;

export const DemoDropdown = styled(Dropdown)`
  margin-right: 10px;
`;
