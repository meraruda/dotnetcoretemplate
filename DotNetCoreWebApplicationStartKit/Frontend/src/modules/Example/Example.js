import React, { Component, Fragment } from 'react';
import produce from 'immer';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Page, ListProvider, Query } from '~~compose/';
import { Form, Modal, Input, Button } from '~~atoms';
import * as Style from './Style';

const INIT_MODAL = {
  visible: false,
  name: '',
  age: '',
  address: '',
};

const INIT_QUERY = {
  name: '',
};

const SignupSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  age: Yup.number()
    .required('Required'),
  address: Yup.string()
    .required('Required'),
});

export default class Example extends Component {
  state = {
    modal: INIT_MODAL,
    query: INIT_QUERY,
  }

  fakeApi = (payload) => {
    console.log('payload', payload);

    const fakeData = [{
      key: '1',
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park',
      tags: ['nice', 'developer'],
    }, {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park',
      tags: ['loser'],
    }, {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park',
      tags: ['cool', 'teacher'],
    }];

    const data = fakeData.filter(o => o.name.toLocaleLowerCase().indexOf(payload.query.name.toLocaleLowerCase()) !== -1);

    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          data,
          pageResult: {
            ...payload.pagination,
            totalNumber: data.length,
          },
        });
      }, 1000);
    });
  }

  onChangeModal = (key, value) => {
    this.setState(state => produce(state, (draftState) => {
      draftState.modal[key] = value;
    }));
  }

  initModal = () => {
    this.setState({
      modal: INIT_MODAL,
    });
  }

  handleAdd = (values, { resetForm }) => {
    console.log('modal values', values);
    resetForm(INIT_MODAL);
    this.initModal();
  }

  render() {
    const { modal, query } = this.state;

    const tableHeader = [
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        sorter: true,
        align: 'center',
      },
      {
        title: 'Age',
        dataIndex: 'age',
        key: 'age',
        align: 'center',
      },
      {
        title: 'Address',
        dataIndex: 'address',
        key: 'address',
        align: 'center',
      },
      {
        title: 'Tags',
        key: 'tags',
        dataIndex: 'tags',
        align: 'center',
        render: tags => (
          <span>
            {tags.join(', ')}
          </span>
        ),
      },
    ];

    return (
      <Page.Main title="範例頁">
        <ListProvider.Main
          searchKey="ExampleList"
          getListFunction={payload => this.fakeApi(payload)}
        >
          {({ state, ...list }) => (
            <Fragment>
              <Query.Main
                query={query}
                handleSearch={list.handleSearch}
              >
                {({ state: qs, ...qf }) => (
                  <Page.Row>
                    <Page.Column>
                      <Style.SearchBar>
                        <Input
                          value={qs.name}
                          placeholder="請輸入姓名"
                          handleChange={value => qf.changeQuery('name', value)}
                        />
                        <Button onClick={() => { qf.onSearch(); qf.onReset(); }}>查詢</Button>
                      </Style.SearchBar>
                    </Page.Column>
                    <Page.Column align="right">
                      <Button onClick={() => qf.changeQuery('visible', true)}>新增</Button>
                    </Page.Column>
                  </Page.Row>
                )}
              </Query.Main>
              <Page.Row>
                <ListProvider.Table
                  columns={tableHeader}
                />
              </Page.Row>
            </Fragment>
          )}
        </ListProvider.Main>

        <Formik
          initialValues={modal}
          validationSchema={SignupSchema}
          onSubmit={this.handleAdd}
        >
          {({ submitForm, ...restProps }) => (
            <Modal
              title="新增"
              visible={modal.visible}
              onOk={submitForm}
              onCancel={this.initModal}
            >
              <Form.Main data={modal} onChange={this.onChangeModal} {...restProps}>
                <Form.FormInput placeholder="姓名" name="name" />
                <Form.FormInput placeholder="年齡" name="age" />
                <Form.FormInput placeholder="地址" name="address" />
              </Form.Main>
            </Modal>
          )}
        </Formik>
      </Page.Main>
    );
  }
}
