import styled from 'styled-components';

export const SearchBar = styled.div`
  display: flex;

  .ant-input-affix-wrapper {
    width: 290px;
    margin-right: 20px;
  }
`;
