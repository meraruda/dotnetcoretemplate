import styled from 'styled-components';

export const FullPageDiv = styled.div`
  width: 100vw;
  height: 100vh;
  min-height: 650px;
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;
  background-color: ${({ theme }) => theme.bg_full_page};
  color: #fff;

  footer {
    width: 100%;
    padding: 10px 0;
    background-color: ${({ theme }) => theme.bg_footer};
    position: absolute;
    bottom: 0px;
    text-align: center;
  }
`;

export const Brand = styled.div`
  color: #FFF;
  margin-top: 195px;
  margin-bottom: 25px;
  font-size: 38px;
  font-weight: 400;
`;
