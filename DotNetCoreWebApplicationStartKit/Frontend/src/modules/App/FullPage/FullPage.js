import React from 'react';

import AppConfig from '~~config/';
import { FullpageRoutes } from '~~routes/';

import * as Style from './FullPageStyle';

const FullPage = () => (
  <Style.FullPageDiv>
    <Style.Brand>
      React Startkit
    </Style.Brand>
    <FullpageRoutes />
    <footer>
      COPYRIGHT ©2019 WISTRON CORPORATION. All RIGHTS RESERVED.
      {AppConfig.version}
    </footer>
  </Style.FullPageDiv>
);

export default FullPage;
