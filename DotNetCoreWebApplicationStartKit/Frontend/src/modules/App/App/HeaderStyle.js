import styled from 'styled-components';

export const AppHeader = styled.div`
  width: 100%;
  position: fixed;
  height: 60px;
  z-index: 989;
`;

export const AppToolBar = styled.div`
  display: flex;
  width: 100%;
  height: 64px;
  background-color: ${({ theme }) => theme.bg_header};
  padding: 0 80px;
  color: #fff;
  align-items: center;
  justify-content: space-between;
`;

export const AppToolbox = styled.div`
  display: flex;
  align-items: center;

  > * {
    margin-right: 20px;

    &:last-child {
      margin-right: 0px;
    }
  }

  > .ant-dropdown-link {
    display: flex;
    align-items: center;
    color: #fff;
    border: 1px solid #fff;
    border-radius: 3px;
    padding: 3px 10px;

    .anticon {
      margin-left: 10px;
    }

    svg {
      width: 12px;
      height: 12px;
    }
  }
`;

export const UserName = styled.span`
  color: #cacaca;
`;

export const AppBrand = styled.div`
  font-size: 18px;
  width: 220px;
  display: flex;

`;

export const AppBrandTitle = styled.span`
  color: #fff;
`;

export const AppBreadcrumb = styled.div`
  width: 100%;
  height: 30px;
  background-color: ${({ theme }) => theme.bg_full_page};
  padding: 0 80px;
  color: #fff;
  display: flex;
  align-items: center;
  font-size: 14px;
`;

export const BreadcrumbDivider = styled.span`
  padding: 0 10px;
`;
