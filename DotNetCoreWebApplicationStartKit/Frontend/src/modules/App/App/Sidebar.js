import React from 'react';
import { withRouter } from 'react-router-dom';
import { PATH } from '~~utils/';
import { useTranslation } from 'react-i18next';

import * as Style from './SidebarStyle';

const menu = [
  { key: 'EXAMPLE', path: PATH.EXAMPLE },
  { key: 'STORE', path: PATH.STORE },
  { key: 'DEMO', path: PATH.DEMO },
  { key: 'HOOK_EXAMPLE', path: PATH.HOOK_EXAMPLE },
];


const Sidebar = withRouter(({ history }) => {
  const handleClick = (e) => {
    const matchRoute = menu.find(o => o.key === e.key);
    if (matchRoute) history.push(matchRoute.path);
  };

  const { t } = useTranslation();

  return (
    <Style.NavBar
      mode="inline"
      onClick={handleClick}
    >
      {menu.map(m => (
        <Style.NarItem key={m.key}>
          {t(`MENU.${m.key}`)}
        </Style.NarItem>
      ))}
    </Style.NavBar>
  );
});

export default Sidebar;
