import styled from 'styled-components';
// import { Col } from 'reactstrap';

export const Container = styled.div`
  background-color: ${({ theme }) => theme.bg_app};

  footer {
    width: 100%;
    padding: 10px 0;
    background-color: ${({ theme }) => theme.bg_footer};
    position: absolute;
    bottom: 0px;
    text-align: center;
    color: #fff;
  }
`;

export const Content = styled.div`
  display: flex;
  padding-left: 60px;
  padding-top: 94px;
  background-color: ${({ theme }) => theme.bg_app};
  min-height: calc(100vh - 30px);
  min-width: 1080px;
`;

export const SidebarCol = styled.div`
  width: 200px;
  background-color: #fff;
  border-left: 1px #dcdcdc solid;
  border-right: 1px #dcdcdc solid;
  padding: 0px;
`;

export const ContentCol = styled.div`
  width: calc(100vw - 300px);
  min-width: 1100px;
  padding: 20px 40px;
`;
