import React, { useState, useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';

import { PATH } from '~~utils/';
import { Icon, Menu, Dropdown } from '~~atoms/';
import languageList from '~~locales';
import { iconHome, iconLogOut } from '~~static/icons/';
import { UserContext } from '../../../store/user2Store';

import * as Style from './HeaderStyle';

function getKeyByValue(obj, value) {
  for (const prop in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, prop)) {
      if (obj[prop] === value) return prop;
    }
  }
}

const Header = withRouter(({
  match,
  history,
}) => {
  const { t, i18n } = useTranslation();
  const [isOpenSetting, setIsOpenSetting] = useState(false);
  const userStore = useContext(UserContext);
  const { setLocale, locale, setLogin } = userStore;

  const getBreadcrumb = () => {
    const currentUrl = match.path;
    const i18nKey = `BREADCRUMB.${getKeyByValue(PATH, currentUrl)}`;
    const i18bValue = t(i18nKey);

    const returnComponent = [];

    if (i18bValue) {
      const tmpArray = i18bValue.split(';');

      tmpArray.forEach((item, index) => {
        returnComponent.push(<div key={Math.random()}>{item}</div>);

        if (index !== tmpArray.length - 1) {
          returnComponent.push(
            <Style.BreadcrumbDivider key={Math.random()}>
              &gt;
            </Style.BreadcrumbDivider>,
          );
        }
      });
    }

    return returnComponent;
  };

  const loadLocales = ({ key }) => {
    i18n.changeLanguage(key);
    setLocale(key);
  };

  const handleLogout = () => {
    setLogin(false);
    history.push(PATH.LOGIN);
  };

  const toggleSetting = () => setIsOpenSetting(!isOpenSetting);

  const localeComp = (
    <Menu onClick={loadLocales}>
      {Object.keys(languageList).map(lang => (<Menu.Item key={lang}>{languageList[lang]}</Menu.Item>))}
    </Menu>
  );

  return (
    <Style.AppHeader>
      <Style.AppToolBar>
        <Style.AppBrand>
          <Style.AppBrandTitle>React Startkit</Style.AppBrandTitle>
        </Style.AppBrand>
        <Style.AppToolbox>
          <Style.UserName>Heney HH Yang</Style.UserName>
          <Dropdown overlay={localeComp}>
            <a className="ant-dropdown-link">
              {languageList[locale]}<Icon type="down" />
            </a>
          </Dropdown>
          <Icon type="setting" onClick={toggleSetting} />
          <Icon component={iconHome} />
          <Icon onClick={handleLogout} component={iconLogOut} />
        </Style.AppToolbox>
      </Style.AppToolBar>
      <Style.AppBreadcrumb>{getBreadcrumb()}</Style.AppBreadcrumb>
    </Style.AppHeader>
  );
});

export default Header;
