import React from 'react';
// import PropTypes from 'prop-types';
// import { LocaleProvider } from 'antd';
// import zhTW from 'antd/lib/locale-provider/zh_TW';

// import { CommonHelper } from '~~utils/';
// import { Loader, Notification } from '~~atoms/';
// import { isLogin } from '~~utils/Auth';
import { Consumer } from '~~store/';
import { AppRoutes } from '~~routes/';

import AppConfig from '~~config';
import Header from './Header';
import Sidebar from './Sidebar';

import * as Style from './AppStyle';

const App = () => (
  <Style.Container>
    <Header />
    <Style.Content>
      <Style.SidebarCol>
        <Sidebar />
      </Style.SidebarCol>
      <Style.ContentCol>
        <AppRoutes />
      </Style.ContentCol>
    </Style.Content>
    <footer>
      COPYRIGHT ©2019 WISTRON CORPORATION. All RIGHTS RESERVED.
      {AppConfig.version}
    </footer>
  </Style.Container>
);

export default Consumer('loaderStore', 'notificatonStore', 'userStore')(App);
