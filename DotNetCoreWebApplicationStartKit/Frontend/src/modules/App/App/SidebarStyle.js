import styled from 'styled-components';
// import { NavLink } from 'react-router-dom';
import { Menu } from '~~atoms';
import theme from '../../../theme';

const { SubMenu, Item } = Menu;

export const NavBar = styled(Menu)`
  li {
    display: flex !important;
    align-items: center !important;
    height: 50px !important;
    margin: 0 !important;
    border-bottom: 1px solid rgb(180, 180, 181);
  }

  li:first-child {
    margin-top: 20px !important;
    border-top: 1px solid rgb(180, 180, 181);
  }
`;

export const SubNav = styled(SubMenu)`
  &:hover {
    background: ${theme['menu-hover-bg-color']};
  }
`;

export const NarItem = styled(Item)`
  &:hover {
    background: ${theme['menu-hover-bg-color']};
  }
`;
