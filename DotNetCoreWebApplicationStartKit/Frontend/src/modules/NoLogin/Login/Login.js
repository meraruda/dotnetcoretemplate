import React, { Component } from 'react';
import { CommonHelper, PATH } from '~~utils/';
import { Input, Button, Icon } from '~~atoms/';
import accountIcon from '~~static/images/account.svg';
import passwordIcon from '~~static/images/password.svg';
import { Consumer } from '~~store/';
import * as Style from './style';

@Consumer('userStore')
@CommonHelper()
class Login extends Component {
  state = {
    account: '',
    password: '',
  }

  handleChange = (key, value) => {
    this.setState({
      [key]: value,
    });
  }

  handleLogin = () => {
    const { account, password } = this.state;
    const { history, context: { userStore } } = this.props;
    console.log(account, password);

    userStore.setLogin(true);
    history.push(PATH.EXAMPLE);
  }

  render() {
    const { account, password } = this.state;
    const { t } = this.props;

    return (
      <Style.LoginContainer>
        <Style.LoginForm>
          <Style.ErrorMessage>
            <Icon type="warning" />
            <span>
              帳號密碼錯誤
            </span>
          </Style.ErrorMessage>
          <Style.LoginInputRow>
            <Input
              placeholder={t('COMMON.ENTER_THE_ACCOUNT')}
              icon={accountIcon}
              value={account}
              handleChange={value => this.handleChange('account', value)}
            />
          </Style.LoginInputRow>
          <Style.LoginInputRow>
            <Input
              placeholder={t('COMMON.ENTER_THE_PASSWORD')}
              type="password"
              icon={passwordIcon}
              value={password}
              handleChange={value => this.handleChange('password', value)}
            />
          </Style.LoginInputRow>
          <Button onClick={this.handleLogin}>
            {t('COMMON.LOGIN')}
          </Button>
        </Style.LoginForm>
      </Style.LoginContainer>
    );
  }
}

export default Login;
