import styled from 'styled-components';

export const LoginContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const LoginForm = styled.div`
  width: 420px;
  background-color: #e6e6e6;
  border-radius: 10px;
  padding: 62px 30px 45px 30px;
  display: flex;
  flex-direction: column;
  align-items: center;

  button {
    width: 261px;
    margin-top: 40px;
  }
`;

export const LoginInputRow = styled.div`
  width: 100%;
  height: 50px;
`;


export const ErrorMessage = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 20px;
  color: #ed5b00;
  text-align: left;

  svg {
    margin-right: 5px;
  }
`;
