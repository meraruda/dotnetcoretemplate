import { useState } from 'react';

const useModal = () => {
  const [visible, setVisible] = useState(false);
  const [modalData, setModalData] = useState(null);

  function openModal(data) {
    setVisible(true);
    setModalData(data);
  }

  function closeModal() {
    setVisible(false);
    setModalData(null);
  }

  return {
    visible,
    modalData,
    openModal,
    closeModal,
  };
};

export default useModal;
