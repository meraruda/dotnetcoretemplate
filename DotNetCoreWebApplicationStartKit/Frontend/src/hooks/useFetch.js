import { useState } from 'react';

const useFetch = (
  fetchFn,
  ininData,
) => {
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [error, setError] = useState(null);
  const [data, setData] = useState(ininData);

  async function fetchData(payload) {
    setIsLoading(true);

    try {
      const response = await fetchFn(payload);
      setData(response);
    } catch (err) {
      console.error('[Fetch data error]: ', err.message);
      setError(err.message);
      setIsError(true);
    }

    setIsLoading(false);
  }

  return {
    isLoading,
    isError,
    data,
    error,
    fetchData,
  };
};

export default useFetch;
