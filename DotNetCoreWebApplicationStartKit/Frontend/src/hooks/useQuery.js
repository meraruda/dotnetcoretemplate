import { useState } from 'react';

const DEFAULT_PAGINATION = {
  current: 0,
  pageSize: 10,
  totalNumber: 1,
};

const DEFAULT_SORTER = {
  field: null,
  type: 'asc',
};

const useQuery = (
  initQuery,
  initPagination = DEFAULT_PAGINATION,
  initSorter = DEFAULT_SORTER,
) => {
  const [query, updateQuery] = useState(initQuery);
  const [pagination, updatePagination] = useState(initPagination);
  const [sorter, updateSorter] = useState(initSorter);

  const payload = {
    query,
    pagination,
    sorter,
  };

  const setQuery = (newState) => {
    updateQuery({
      ...query,
      ...newState,
    });
  };
  const setPagination = (newState) => {
    updatePagination({
      ...pagination,
      ...newState,
    });
  };
  const setSorter = (newState) => {
    updateSorter({
      ...query,
      ...newState,
    });
  };

  const resetQuery = () => updateQuery(initQuery);

  return {
    query,
    setQuery,
    resetQuery,
    pagination,
    setPagination,
    sorter,
    setSorter,
    payload,
  };
};

export default useQuery;
