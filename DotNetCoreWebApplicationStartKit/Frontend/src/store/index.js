import React, { Component, createContext } from 'react';
import produce from 'immer';

import userStore from './userStore';
import countStore from './countStore';
import loaderStore from './loaderStore';
import notificationStore from './notificationStore';
// import user2Store from './user2Store';

const myContext = createContext();

export default class Provider extends Component {
  state = {
    ...userStore(this),
    ...countStore(this),
    ...loaderStore(this),
    ...notificationStore(this),
  };

  componentDidMount () {
    this.getExposeState();
  }

  /**
   * 將部分store暴露到browser 的 global 裡面
   */
  getExposeState = () => {
    const exposeState = {
      notificatonStore: this.state.notificatonStore
    }

    global.state = exposeState;
  }

  render() {
    return (
      <myContext.Provider value={this.state}>
        {this.props.children}
      </myContext.Provider>
    );
  }
}

export const Consumer = (...stores) => (ComposedComponent) => {
  class ConsumerComponent extends Component {
    static contextType = myContext;

    render() {
      if (stores.length === 0) return <ComposedComponent {...this.props} context={this.context} />;

      const newContext = Object.keys(this.context).reduce((acc, key) => {
        if (stores.includes(key)) return { ...acc, [key]: this.context[key] };
        return acc;
      }, {});

      return (
        <ComposedComponent {...this.props} context={newContext} />
      );
    }
  }
  return ConsumerComponent;
};

export { default as user2Store }  from './user2Store';
