/* eslint no-eval: 0 */
import produce from 'immer';

const toggleNotification = (self, isOpen, message, messageVariable, callback) => {
  self.setState(state => produce(state, (draftState) => {
    draftState.notificatonStore.isOpen = isOpen;
    draftState.notificatonStore.message = message;
    draftState.notificatonStore.messageVariable = messageVariable;
    draftState.notificatonStore.callback = callback;
  }));
};

export default self => ({
  notificatonStore: {
    isOpen: false,
    message: '',
    messageVariable: {},
    callback: () => {},
    openNotification: (message, messageVariable = {}, callback = () => {}) => toggleNotification(self, true, message, messageVariable, callback),
    closeNotification: () => toggleNotification(self, false, '', {}, () => {}),
  },
});
