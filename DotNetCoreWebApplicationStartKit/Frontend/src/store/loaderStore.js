/* eslint no-eval: 0 */
import produce from 'immer';

const ACTION = {
  OPEN: '+',
  CLOSE: '-',
};

const toggleLoader = (self, action) => {
  self.setState(state => produce(state, (draftState) => {
    const count = eval(`${draftState.loaderStore.loaderCount} ${action} 1`);
    const loaderCount = count < 0
      ? 0
      : count;

    draftState.loaderStore.loaderCount = loaderCount;
    draftState.loaderStore.isLoading = loaderCount > 0;
  }));
};

export default self => ({
  loaderStore: {
    target: self,
    isLoading: false,
    loaderCount: 0,
    openLoader: () => toggleLoader(self, ACTION.OPEN),
    closeLoader: () => toggleLoader(self, ACTION.CLOSE),
  },
});
