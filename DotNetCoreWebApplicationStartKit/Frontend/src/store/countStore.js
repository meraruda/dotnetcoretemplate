import produce from 'immer';

const increment = (self) => {
  self.setState(state => produce(state, (draftState) => {
    draftState.countStore.count += 1;
  }));
};

const decrement = (self) => {
  self.setState(state => produce(state, (draftState) => {
    draftState.countStore.count -= 1;
  }));
};

export default self => ({
  countStore: {
    count: 1,
    increment: () => increment(self),
    decrement: () => decrement(self),
  },
});
