import produce from 'immer';

const setLogin = (self, value) => {
  self.setState(state => produce(state, (draftState) => {
    draftState.userStore.isLogin = value;
  }));
};

const setLocale = (self, value) => {
  self.setState(state => produce(state, (draftState) => {
    draftState.userStore.locale = value;
  }));
};

export default self => ({
  userStore: {
    isLogin: false,
    setLogin: value => setLogin(self, value),
    locale: 'zh-TW',
    setLocale: value => setLocale(self, value),
  },
});
