import React, { useState, createContext } from 'react';

export const UserContext = createContext();

export const UserContextProvider = ({ children }) => {
  const [isLogin, setLogin] = useState(false);
  const [locale, setLocale] = useState('zh-TW');

  return (
    <UserContext.Provider
      value={{
        isLogin,
        setLogin,
        locale,
        setLocale,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export default UserContextProvider;
