﻿using Castle.DynamicProxy;
using System;

namespace DotNetCoreWebApplicationStartKit.Interceptor
{
    public class SampleInterceptor : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            Console.WriteLine("Do some thing before enter action");
            invocation.Proceed();
        }
    }
}
