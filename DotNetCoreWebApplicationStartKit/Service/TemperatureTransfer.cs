﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCoreWebApplicationStartKit.Service
{
    public class TemperatureTransfer
    {
        public static int CtoF(int TemperatureC) {
            return 32 + (int)(TemperatureC / 0.5556);
        }
    }
}
