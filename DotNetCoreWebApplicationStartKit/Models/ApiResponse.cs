﻿using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;

namespace DotNetCoreWebApplicationStartKit.Models.ApiResponse
{
    public class WeatherForecastExample : IExamplesProvider<List<WeatherForecast>>
    {
        public List<WeatherForecast> GetExamples()
        {
            return new List<WeatherForecast>
            {
                new WeatherForecast
                {
                    DateFormatted = DateTime.Now.ToString("d"),
                    TemperatureC = 27,
                    Summary = "rainy, cloudy",
                }
            };
        }
    }
}
