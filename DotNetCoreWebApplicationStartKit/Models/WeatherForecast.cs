﻿using DotNetCoreWebApplicationStartKit.Service;
using System.ComponentModel.DataAnnotations;

namespace DotNetCoreWebApplicationStartKit.Models
{
    public class WeatherForecast
    {
        [Required]
        public string DateFormatted { get; set; }
        [Required]
        public int TemperatureC { get; set; }
        public string Summary { get; set; }

        public int TemperatureF
        {
            get
            {
                return TemperatureTransfer.CtoF(TemperatureC);
            }
        }
    }
}
