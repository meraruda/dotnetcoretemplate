﻿using Swashbuckle.AspNetCore.Filters;
using System;
using System.ComponentModel.DataAnnotations;

namespace DotNetCoreWebApplicationStartKit.Models.ApiRequest
{
    public class LoginRequest
    {
        /// <summary>
        /// user account
        /// </summary>
        [Required]
        public string Account { get; set; }
        /// <summary>
        /// user password
        /// </summary>
        [Required]
        public string Password { get; set; }
    }

    public class GetWeatherForecastRequest
    {
        /// <summary>
        /// date to query - 1558281600000
        /// </summary>
        [Required]
        public long date { get; set; }
    }

    public class PostWeatherForecastRequestExample : IExamplesProvider<WeatherForecast>
    {
        public WeatherForecast GetExamples() => new WeatherForecast
        {
            DateFormatted = DateTime.Now.ToString("d"),
            TemperatureC = 27,
            Summary = "rainy, cloudy",
        };
    }
}
